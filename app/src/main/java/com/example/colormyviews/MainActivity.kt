package com.example.colormyviews

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setListeners()
    }
    private fun setListeners(){
        val clickableViews: List<View> =
                listOf(box1,box2,box3,box4,box5
                ,button_red,button_green,button_yellow)
        for(item in clickableViews) {
            item.setOnClickListener{
                makeColored(it)
            }
        }
    }
    private
    fun makeColored(view: View){
        when (view.id) {
            R.id.box1 -> view.setBackgroundColor(Color.DKGRAY)
            R.id.box2 -> view.setBackgroundColor(Color.GRAY)

            R.id.box3 -> view.setBackgroundResource(android.R.color.holo_green_light)
            R.id.box4 -> view.setBackgroundResource(android.R.color.holo_green_dark)
            R.id.box5 -> view.setBackgroundResource(android.R.color.holo_green_light)

            //view คือเปลี่ยนสีของตัวปุ่มเองถ้าเปลี่ยน view เป็นเหมือนตัวปุ่มข้างล่างจะทำการเปลี่ยนสีปุ่มเที่เราลล็อคเอาไว้แทน
            /*R.id.button_red -> view.setBackgroundResource(R.color.my_red)
            R.id.button_yellow -> view.setBackgroundResource(R.color.my_yellow)
            R.id.button_green -> view.setBackgroundResource(R.color.my_green)*/

            R.id.button_red -> box3.setBackgroundResource(R.color.my_red)
            R.id.button_yellow -> box4.setBackgroundResource(R.color.my_yellow)
            R.id.button_green -> box5.setBackgroundResource(R.color.my_green)

            else -> view.setBackgroundColor(Color.LTGRAY)
        }
    }
}